package design.lanmu;

import design.lanmu.dispatch.DispatchServlet;

import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
    public static void main(String[] args) throws Exception {
        ServerSocket server = new ServerSocket(8001);
        while (true) {
            DispatchServlet dispatchServlet = new DispatchServlet();
            Socket client = server.accept();
            dispatchServlet.dispatch(client);
        }
    }
}
