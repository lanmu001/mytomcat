package design.lanmu.dispatch;

import cn.hutool.core.util.StrUtil;
import design.lanmu.annotation.Servlet;
import design.lanmu.http.HttpRequest;
import design.lanmu.http.HttpResponse;
import design.lanmu.http.socket.RequestReader;
import design.lanmu.http.socket.ResponseWriter;
import design.lanmu.servlet.HttpService;
import design.lanmu.servlet.HttpServlet;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;
import java.net.URL;
import java.util.*;

public class DispatchServlet {

    private static final String PACKAGE_NAME = "design.lanmu";

    Stack<String> classpathStack = new Stack<>();

    Map<String,HttpService> services = new HashMap<>(30);





    public void dispatch(Socket client) throws Exception {
        HttpRequest request = new RequestReader(client).getRequest();
        HttpResponse response = new ResponseWriter(client).getResponse();
        String path = request.getPath();
        if (path.contains("?")) {
            path = path.substring(0, path.indexOf("?"));
        }
        HttpService httpService = services.get(path);
        if (httpService == null) {
            httpService = services.get(path.substring(1, path.length() - 1));
            if (httpService == null) {
                response.sendError(404, "Servlet 没有找到！");
                return;
            }
        } else {
            if (httpService instanceof HttpServlet) {
                HttpServlet servlet = (HttpServlet) httpService;
                if ("GET".equalsIgnoreCase(request.getMethod())) {
                    servlet.doGet(request, response);
                }
                if ("POST".equalsIgnoreCase(request.getMethod())) {
                    servlet.doPost(request, response);
                }
            } else {
                httpService.service(request, response);
            }
        }
    }

    public DispatchServlet() throws Exception {
        loadServices(PACKAGE_NAME);
    }


    void loadServices(List<String> classpath) throws Exception {
        for (String s : classpath) {
            Class aClass = Class.forName(s);
            Servlet declaredAnnotation = Class.forName(s).getDeclaredAnnotation(Servlet.class);
            if (declaredAnnotation != null) {
                String path = declaredAnnotation.path();
                if (StrUtil.isBlank(path)) {
                    path = declaredAnnotation.value();
                }
                Object service = aClass.getConstructor().newInstance();
                if (service instanceof HttpService) {
                    HttpService httpService = (HttpService) service;
                    httpService.init();
                    services.put(path, httpService);
                }
            }
        }
    }


    void loadServices(String packageName) throws Exception {
        URL resource = getClass().getClassLoader().getResource("");
        String path = resource.getPath();
        String parentPath = path.substring(1, path.length() - 1) + "/" + packageName.replace(".", "/");
        File[] files = new File(parentPath).listFiles();
        List<String> list = new ArrayList<>();
        classpathStack.push(packageName);
        for (File file : files) {
            treeDir(file, list);
            classpathStack.clear();
            classpathStack.push(packageName);
        }
        loadServices(list);
    }


    void treeDir(File f, List<String> list) {
        if (f.isFile()) {
            String filename = f.getName().substring(0, f.getName().indexOf("."));
            classpathStack.push(filename);
            StringJoiner stringJoiner = new StringJoiner(".");
            for (String s : classpathStack) {
                stringJoiner.add(s);
            }
            list.add(stringJoiner.toString());
            classpathStack.pop();
        } else {
            File[] files = f.listFiles();
            classpathStack.push(f.getName());
            if (files != null) {
                for (File file : files) {
                    treeDir(file, list);
                }
            }
            classpathStack.pop();
        }
    }
}
