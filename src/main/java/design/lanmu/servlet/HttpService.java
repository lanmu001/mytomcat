package design.lanmu.servlet;

import design.lanmu.http.HttpRequest;
import design.lanmu.http.HttpResponse;

import java.io.IOException;

public interface HttpService {

    void init();

    void service(HttpRequest request, HttpResponse response) throws IOException;

    void destroy();
}
