package design.lanmu.servlet.impl;

import design.lanmu.annotation.Servlet;
import design.lanmu.http.HttpRequest;
import design.lanmu.http.HttpResponse;
import design.lanmu.servlet.HttpService;

import java.io.IOException;

public abstract class BaseServlet implements HttpService {
    @Override
    public void init() {

    }

    @Override
    public void service(HttpRequest request, HttpResponse response) throws IOException {

    }

    @Override
    public void destroy() {

    }
}
