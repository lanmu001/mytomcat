package design.lanmu.servlet;

import design.lanmu.http.HttpRequest;
import design.lanmu.http.HttpResponse;

public interface HttpServlet extends HttpService{
    void doGet(HttpRequest request, HttpResponse response);
    void doPost(HttpRequest request, HttpResponse response);
}
