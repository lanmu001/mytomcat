package design.lanmu.http.impl;

import design.lanmu.http.HttpResponse;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class HttpResponseImpl implements HttpResponse {

    public HttpResponseImpl() {

    }

    private static  final Map<Integer, HttpStatus> statusMap = new HashMap<>();

    static {
        for (HttpStatus httpStatus : HttpStatus.values()) {
            statusMap.put(httpStatus.code, httpStatus);
        }
    }

    private OutputStream outputStream;

    Map<String, Object> headers = new HashMap<>();


    public OutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    @Override
    public void sendRedirect(String location) {

    }

    @Override
    public void setContentType(String type) {
        setHeader(CONTENT_TYPE, type);
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    @Override
    public OutputStream getOutputSteam() {
        return outputStream;
    }

    private void setHeader(String key, Object value) {
        headers.put(key, value);
    }

    @Override
    public void setHeader(String key, String value) {
        headers.put(key, value);
    }

    @Override
    public void sendError(int sc, String msg) {
        try {
            writer(sc, msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setCharacterEncoding(String charset) {
        setHeader(CHARACTER_ENCODING, charset);
    }

    @Override
    public void setContentLength(int len) {
        setHeader(CONTENT_LENGTH, len);
    }

    @Override
    public void writer(String line) throws IOException {
        writer(200, line);
    }

    private void writer(int code, String line) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(getOutputSteam()));
        StringBuilder builder = new StringBuilder();
        HttpStatus status = getHttpStatus(code);
        builder.append("<html>")
                .append("<head>")
                .append("<title>"+status.code+ "\t" + status.name() + "</title>")
                .append("<meta charset=\"utf-8\"/>")
                .append("</head>")
                .append("<body>")
                .append("<h1>" + line + "</h1>")
                .append("</body>")
                .append("</html>");
        bufferedWriter.write(getResponseContent(code, builder.toString()));
        bufferedWriter.flush();
    }




    String getResponseContent(int code, String msg) {
        StringJoiner builder = new StringJoiner("\r\n");
        HttpStatus status = getHttpStatus(code);
        builder.add("HTTP/1.1 " + status.code + status.name().toLowerCase());
        builder.add("Date: " + new Date());
        builder.add("Server: " + "MyTomcat/0.0.1;charset=UTF-8");
        builder.add(HttpResponse.CONTENT_TYPE+": " + HttpResponse.TEXT_HTML);
        builder.add(HttpResponse.CONTENT_LENGTH+": " + msg.getBytes().length);
        headers.forEach((key, value) -> {
            builder.add(key + ": " + value);
        });
        builder.add("");
        builder.add(msg);
        return builder.toString();
    }

    private static enum HttpStatus {
        OK(200), ERROR(500), NOT_FOUND(404);

        int code;

        HttpStatus(int code) {
            this.code = code;
        }
    }

    HttpStatus getHttpStatus(int code) {
        return statusMap.get(code);
    }

}
