package design.lanmu.http.impl;

import design.lanmu.http.HttpRequest;

import java.util.HashMap;
import java.util.Map;

public class HttpRequestImpl implements HttpRequest {
    private String method;
    private String url;
    private String protocol;
    private String host;
    private String path;
    private String contentType;
    private Map<String, Object> params = new HashMap<>();
    private byte[] content;
    private String requestStr;

    public Map<String, Object> getParams() {
        return params;
    }

    public String getRequestStr() {
        return requestStr;
    }

    public void setRequestStr(String requestStr) {
        this.requestStr = requestStr;
    }

    private HttpRequestImpl() {
    }

    private HttpRequestImpl(String method, String url, String protocol, String host, String path, String contentType, Map<String, Object> params, byte[] content) {
        this.method = method;
        this.url = url;
        this.protocol = protocol;
        this.host = host;
        this.path = path;
        this.contentType = contentType;
        this.params = params;
        this.content = content;
    }

    @Override
    public byte[] getContent() {
        return content;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public Object getParameterValue(String key) {
        return params.getOrDefault(key, "");
    }


    @Override
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    @Override
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String method;
        private String url;
        private String protocol;
        private String host;
        private String path;
        private String contentType;
        private Map<String, Object> params = new HashMap<>();
        private String requestStr;
        private byte[] content;

        public Builder method(String method) {
            this.method = method;
            return this;
        }
        public Builder url(String url) {
            this.url = url;
            return this;
        }
        public Builder protocol(String protocol) {
            this.protocol = protocol;
            return this;
        }
        public Builder host(String host) {
            this.host = host;
            return this;
        }
        public Builder path(String path) {
            this.path = path;
            return this;
        }
        public Builder contentType(String contentType) {
            this.contentType = contentType;
            return this;
        }
        public Builder params(Map<String, Object> params) {
            this.params = params;
            return this;
        }
        public Builder content(byte[] content) {
            this.content = content;
            return this;
        }
        public Builder requestStr(String requestStr) {
            this.requestStr = requestStr;
            return this;
        }

        public HttpRequestImpl build() {
            HttpRequestImpl httpRequest = new HttpRequestImpl
                    (
                            this.method,
                            this.url,
                            this.protocol,
                            this.host,
                            this.path,
                            this.contentType,
                            this.params,
                            this.content
                    );
            httpRequest.setRequestStr(requestStr);
            return httpRequest;
        }
    }

}
