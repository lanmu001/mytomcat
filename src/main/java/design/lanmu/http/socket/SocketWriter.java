package design.lanmu.http.socket;

import design.lanmu.http.HttpResponse;

import java.io.IOException;

public interface SocketWriter {

    HttpResponse getResponse() throws IOException;

}
