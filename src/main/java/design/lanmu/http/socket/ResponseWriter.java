package design.lanmu.http.socket;

import design.lanmu.http.HttpResponse;
import design.lanmu.http.impl.HttpResponseImpl;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class ResponseWriter extends AbstractSocket implements SocketWriter{

    public ResponseWriter(Socket socket) {
        super(socket);
    }

    @Override
    public HttpResponse getResponse() throws IOException {
        if (isConnected()) {
            HttpResponseImpl response = new HttpResponseImpl();
            response.setOutputStream(socket.getOutputStream());
            return response;
        }
        throw new SocketException("socket 连接断开");
    }
}
