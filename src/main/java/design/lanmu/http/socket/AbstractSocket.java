package design.lanmu.http.socket;

import cn.hutool.log.Log;
import cn.hutool.log.dialect.log4j.Log4jLog;

import java.net.Socket;

public abstract class AbstractSocket {

    private Log log = new Log4jLog(AbstractSocket.class);

    protected Socket socket;

    protected AbstractSocket(Socket socket) {
        this.socket = socket;
    }

    protected AbstractSocket() {
    }
    public boolean isConnected() {
        return socket.isConnected();
    }

}
