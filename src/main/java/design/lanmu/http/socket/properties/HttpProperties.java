package design.lanmu.http.socket.properties;

import java.net.Socket;
import java.util.List;
import java.util.Map;

public interface HttpProperties {

    String REQUEST_METHOD = "method";
    String REQUEST_PATH = "path";
    String REQUEST_PROTOCOL = "protocol";
    String REQUEST_HOST = "host";
    String REQUEST_CONTENT_TYPE = "contextType";
    String REQUEST_URL = "url";
    String REQUEST_PARAMS = "params";
    String REQUEST_CONTENT = "content";

    Map<String, String> readProperties();

}
