package design.lanmu.http.socket.properties.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;
import design.lanmu.http.socket.properties.HttpProperties;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpPropertiesImpl  implements HttpProperties {

    private Socket socket;


    public HttpPropertiesImpl(Socket socket) {
        this.socket = socket;
    }

    @Override
    public Map<String, String> readProperties() {
        return readRequestProperties();
    }

    private List<String> getRequestLine(Socket socket) {
        final List<String> requestLine = new ArrayList<>(10);
        if (socket.isConnected()) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String str = "";
                while (!(str = bufferedReader.readLine()).equals("")) {
                    requestLine.add(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return requestLine;
    }


    public Map<String, String> readRequestProperties() {
        Map<String, String> properties = new HashMap<>(10);
        List<String> requestLine = getRequestLine(socket);
        String requestHeader = requestLine.remove(0);
        readRequestHeader(properties, requestHeader);
        for (String s : requestLine) {
            if (StrUtil.isNotBlank(s)) {
                String[] split = s.split(":");
                properties.put(split[0], split[1].trim());
            }
        }
        return properties;
    }

    private void readRequestHeader(Map<String, String> properties, String http) {
        if (StrUtil.isNotBlank(http)) {
            String[] s = http.split(" ");
            properties.put(REQUEST_METHOD, s[0]);
            properties.put(REQUEST_PATH, s[1]);
            properties.put(REQUEST_PROTOCOL, s[2]);
        }
    }

}
