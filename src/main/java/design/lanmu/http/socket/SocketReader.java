package design.lanmu.http.socket;

import design.lanmu.http.HttpRequest;

import java.io.IOException;

public interface SocketReader {



    HttpRequest getRequest();

}
