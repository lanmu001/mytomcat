package design.lanmu.http.socket;

import design.lanmu.exception.SocketException;
import design.lanmu.http.HttpRequest;
import design.lanmu.http.impl.HttpRequestImpl;
import design.lanmu.http.socket.properties.HttpProperties;
import design.lanmu.http.socket.properties.impl.HttpPropertiesImpl;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class RequestReader extends AbstractSocket implements SocketReader{

    Map<String, Object> requestProperties = new HashMap<>(10);

    public RequestReader() {
        throw new SocketException("socket is null");
    }

    public RequestReader(Socket socket) {
        super(socket);
    }

    public Map<String, Object> getRequestParameterMap(String path) {
        Map<String, Object> params = new HashMap<>();
        char[] arr = path.toCharArray();
        for (int i = 0; i < path.toCharArray().length; i++) {
            char c = path.charAt(i);
            String[] properties = new String[2];
            if (c == '?' || c == '&') {
                properties = readPathProperties(arr, i + 1, indexOf(arr, i, '='));
                params.put(properties[0], properties[1]);
            }
        }
        return params;
    }

    /**
     *
     * @param arr 字符数组
     * @param word 想要找到的字符索引
     * @param start 从数组的那个索引开始
     * @return
     */
    private int indexOf(char[] arr, int start, char word) {
        for (int i = start; i < arr.length - 1; i++) {
            if (word == arr[i]) {
                return i;
            }
        }
        return -1;
    }

    /**
     *
     * @param array 读取的PATH字符数组
     * @param start 从数组索引开始读取
     * @param end 设置读取结束索引
     * @param end 设置读取结束索引
     * @return
     */
    private String readPathParameter(char[] array, int start, int end) {
        char[] key = new char[end];
        int index = 0;
        for (int i = start; i < end; i++) {
            key[index++] = array[i];
        }
        return new String(key);
    }

    private String[] readPathProperties(char[] array, int start, int end) {
        String key = readPathParameter(array, start, end);
        int i = indexOf(array, end, '&');
        String value = readPathParameter(array, end + 1, i == -1 ? array.length : i);
        return new String[] { key, value };
    }

    @Override
    public HttpRequest getRequest() {
        HttpProperties httpProperties = new HttpPropertiesImpl(socket);
        Map<String, String> properties = httpProperties.readProperties();
        System.out.println(properties);
        HttpRequestImpl build = HttpRequestImpl.builder()
                .host(properties.getOrDefault(HttpProperties.REQUEST_HOST, "").toString())
                .method(properties.getOrDefault(HttpProperties.REQUEST_METHOD, "").toString())
                .protocol(properties.getOrDefault(HttpProperties.REQUEST_PROTOCOL, "").toString())
                .path(properties.getOrDefault(HttpProperties.REQUEST_PATH, "").toString())
                .url(properties.getOrDefault(HttpProperties.REQUEST_URL, "").toString())
                .params(getRequestParameterMap(properties.get(HttpProperties.REQUEST_PATH)))
                .build();
        return build;
    }
}
