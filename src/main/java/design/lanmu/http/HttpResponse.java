package design.lanmu.http;

import java.io.IOException;
import java.io.OutputStream;

public interface HttpResponse {

    String CONTENT_LENGTH = "Content-Length";
    String CHARACTER_ENCODING = "Character-Encoding";
    String CONTENT_TYPE = "Content_Type";
    String TEXT_HTML = "text/html";

    OutputStream getOutputSteam();

    void setHeader(String key, String value);

    void sendError(int sc, String msg);

    void setCharacterEncoding(String charset);

    void setContentType(String type);

    void setContentLength(int len);

    void writer(String line) throws IOException;

    void sendRedirect(String location);
}
