package design.lanmu.http;

public interface HttpRequest {

    // Post Get
    String getMethod();

    // http:///localhost/index.html
    String getUrl();

    Object getParameterValue(String key);

    // http https 协议
    String getProtocol();

    // localhost 主机
    String getHost();

    // /index.html 路径
    String getPath();

    // application/x-www-form-urlencoded
    String getContentType();

    byte[] getContent();

}
